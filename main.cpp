#include "challenge_info.h"

// ****************************************************************************
// John Quiruz, Aug 28 2023, CS162, Program 5
// This program allows users to manage practice problems related to linear
// linked lists. Users can add, display, and select random practice problems.
// The program offers a user-friendly interface, ensures proper memory 
// management, and promotes modularity through separate source code files.
// ****************************************************************************

int main()
{
	//Variables
	node *head; //pointer that holds entire list
	head = nullptr; //don't point at random memory
	int node_count {0};
	int menu_choice {-1};
	srand(time(0)); //seed the random number generator

	//Control flow of the program
	welcome(); //displays a welcome message and important 
		   //information about the program
	
	//Displays a menu of options for the user to choose from
	main_menu();
	get_menu_choice(menu_choice); //gets the user's choice

	do
	{
		//Build the LLL using data extracted from an external file
		if (BUILD == menu_choice)
			build_list(head, node_count);

		//Display all of the practice problems in the linked list
		else if (DISPLAY_ALL == menu_choice)
			print_challenges(head);

		//Choose a random practice problem from the list
		else if (RANDOM == menu_choice)
			print_random(head, node_count); 

		//Add a practice problem to the linked list
		else if (ADD == menu_choice)
			add_challenge(head, node_count); 

		main_menu();
		get_menu_choice(menu_choice);

	} while (QUIT != menu_choice); //Condition terminates the program

	return 0;
}
