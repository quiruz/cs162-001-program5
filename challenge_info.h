#include <iostream>
#include <cctype>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <fstream>
using namespace std;

// ****************************************************************************
// John Quiruz, Aug 28 2023, CS162, Program 5
// This header file contains data structures and functions used in the program
// for managing practice problems related to linear linked lists. It serves
// as an interface that provides information about the structures used such as:
// The title of the practice problem (e.g., Display All Odd), the details of
// the practice problem (e.g., Output all data that is odd that is contained in
// a linear linked list and return the number of items displayed), prototypes, 
// and level of difficulty.
// ****************************************************************************

// Constants
const int TITLE {101};
const int DETAILS {201};
const int BUILD {1};
const int DISPLAY_ALL {2};
const int RANDOM {3};
const int ADD {4};
const int QUIT {0};

// Structs
struct challenge_info //data about practice problems
{
	char *title; //the title gets dynamically stored
	char details[DETAILS];
	char prototype[DETAILS];
	int difficulty;
	int ordinance;
};

struct node
{
	challenge_info data;
	node *next;
};

// Prototypes
void get_challenge(challenge_info & challenge);
void print_challenge(challenge_info & challenge);
void add_challenge(node * &head, int &node_count);
void print_challenges(node * &head);
void build_list(node * &head, int &node_count);
void print_random(node * &head, int &node_count);
void main_menu();
void get_menu_choice(int &menu_choice);
void welcome();
