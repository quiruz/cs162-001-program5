#include "challenge_info.h"

// ****************************************************************************
// John Quiruz, Aug 28 2023, CS162, Program 5
// This file contains implementations of functions and methods necessary for
// managing practice exercises related to linear linked lists in the context
// of the program. It defines the behavior of key functions such as building
// a linked list, adding an exercise to the list, selecting random problems,
// and displaying all of the exercises contained in each node of the list
// ****************************************************************************

// Functions
//This function is used to store information about an exercise that we
//want to store in our linked list
void get_challenge(challenge_info & challenge)
{
	//Variables
	char temp[TITLE]; //holds a temporary word to be stored
			  //in a dynamic array

	//Reads in information about the practice problem
	cout << "Enter the title of the challenge: ";
	cin.get(temp, TITLE, '\n');
	cin.ignore(100, '\n');

	challenge.title = new char[strlen(temp)];
	strcpy(challenge.title, temp);

	cout << "Enter the details of the challenge: ";
	cin.get(challenge.details, DETAILS, '\n');
	cin.ignore(100, '\n');

	cout << "Enter the prototype for this challenge: ";
	cin.get(challenge.prototype, DETAILS, '\n');
	cin.ignore(100, '\n');

	cout << "Enter the difficulty of the challenge (1 - 5): ";
	cin >> challenge.difficulty;
	cin.ignore(100, '\n');

	//Delete allocated dynamic memory
	//delete challenge.title;
	//challenge.title = nullptr; //put this in main instead?
}



//This function is called whenever we need to display the contents
//of a particular node
void print_challenge(challenge_info & challenge)
{
	//Print all of the information about the practice problem
	cout << "\nChallenge #" << challenge.ordinance
	     << ": " << challenge.title << endl
	     << "Details: " << challenge.details << endl
	     << "Prototype: " << challenge.prototype << endl
	     << "Difficulty" << endl;

	//Convert difficulty from a number to histogram
	for (int i {0}; i < challenge.difficulty; ++i)
		cout << '*';
	cout << endl;
}



//This function adds a node to an empty or already existing list
void add_challenge(node * &head, int &node_count)
{
	//Declare an object of the struct that holds the information
	//about my practice exercises
	challenge_info data;

	//Add a node if the linked list is empty
	if (nullptr == head)
	{
		head = new node;
		++node_count;
		get_challenge(data);
		head->data = data;
		head->data.ordinance = node_count;
		head->next = nullptr;
	}

	//Append a node if the linked list already has nodes
	else 
	{
		//create a pointer and traverse throught the list
		//and then stop at the last node
		node *current = head;
		while (nullptr != current->next)
			current = current->next;

		//create a node; and append it to the end of the 
		//linked list
		node *temp = new node;
		++node_count;
		get_challenge(data); //reads in the exercise
		temp->data = data; //populate the node
		temp->data.ordinance = node_count; //numbers this 
						   //exercise for
						   //organization
		temp->next = current->next; //point at null
		current->next = temp; //safely connect the list
				      //to this new node
	}
}



//This function will have a pointer traverse through a list and 
//print every nodes contents to the user
void print_challenges(node * &head)
{
	//Print this message if the list is empty
	if (nullptr == head)
	{
		cout << "There are no exercises to display." << endl;
		return;
	}
	
	//Traverse through the list, displaying data of each 
	//node along the way
	node *current = head;
	while (nullptr != current)
	{
		print_challenge(current->data);
		current = current->next;
	}
}



//This function reads from an external file and builds the linked list
//filled with practice problems - This is a long function but I don't think 
//working with external data files was a requirement so this was a more
//experimental (and chaotic) function
void build_list(node * &head, int &node_count)
{
	//Variables
	ifstream file_in;
	char temp[TITLE];

	//Read in the file name
	file_in.open("problem_list.txt");

	//Check if the file is connected
	if (!file_in)
		cout << "Unable to connect to \"problem_list.txt\"."
		     << endl;
	else
	{
		if (nullptr == head)
		{
			head = new node;
			++node_count; //Keep track of nodes to refer to them 
				      //numerically

			//This block reads in the title from an external file 
			//and stores into temporary variable and then 
			//converted dynamically to be stored into 
			//a node as the title of the practice problem
			file_in.get(temp, TITLE, '~');
			file_in.ignore(100, '~');
			head->data.title = new char[strlen(temp) + 1];
			strcpy(head->data.title, temp);

			//Read in the rest of the details about each challenge
			file_in.get(head->data.details, DETAILS, '~');
			file_in.ignore(100, '~');
			file_in.get(head->data.prototype, DETAILS, '~');
			file_in.ignore(100, '~');
			file_in >> head->data.difficulty;
			file_in.ignore(100, '\n');
			head->data.ordinance = node_count;

			//Make sure this node is pointing to NULL
			head->next = nullptr;
		}
		for (int i {0}; i < 6; ++i) //Requires prior knowledge of how 
					    //many problems exist in the 
					    //external file
		{
			node *current = head;
			node *new_problem = new node;
			++node_count; //Keep track of nodes to refer to them 
				      //numerically

			while (nullptr != current->next)
				current = current->next;

			//Same process as above
			file_in.get(temp, TITLE, '~');
			file_in.ignore(100, '~');
			new_problem->data.title = new char[strlen(temp) + 1];
			strcpy(new_problem->data.title, temp);

			//Read in the rest of the details about each challenge
			file_in.get(new_problem->data.details, DETAILS, '~');
			file_in.ignore(100, '~');
			file_in.get(new_problem->data.prototype, DETAILS, '~');
			file_in.ignore(100, '~');
			file_in >> new_problem->data.difficulty;
			file_in.ignore(100, '\n');
			new_problem->data.ordinance = node_count;

			//Place the new node at the end of the list
			new_problem->next = current->next;
			current->next = new_problem;
		}
	}
}


//This function displays a welcome message and informs the user
//of the program's purpose
void welcome()
{
	cout << "\nWelcome to the Practice Problem Archive!" << endl;
	cout << "\nExplore, practice, and master linear linked lists exercises "
	     << endl << "with this program. Start by building a list and then choosing "
	     << endl << "any of the options. Let's get started!" << endl;
}



//This function displays the menu interface
void main_menu()
{
	cout << "\nMAIN MENU" << endl << endl;
	cout << "[1] BUILD Problem List" << endl;
	cout << "[2] DISPLAY ALL Problems" << endl;
	cout << "[3] DISPLAY a Random Problem" << endl;
	cout << "[4] ADD a Problem" << endl;
	cout << "[0] QUIT" << endl;
}



//Gets the menu choice input from the user based
void get_menu_choice(int &menu_choice)
{
	cout << "\nEnter your choice: ";
	cin >> menu_choice;
	cin.ignore(100, '\n');
}



//Display a random problem from the list!
void print_random(node * &head, int &node_count)
{
	//Variables
	int random_number {0};

	//Set random number to a variable
	random_number = (rand() % node_count) + 1;

	//Traverse through the linked list and find a match
	//between the random number and the node's index and display
	//that node's practice problem information
	node *current = head;

	//Base case; Make sure the head is NOT NULL
	if (nullptr == head)
	{
		cout << "Nothing in the list to display." << endl;
		return;
	}

	//Traverse
	while (nullptr != current)
	{
		//Once the iteration hits a match display the 
		//practice problem info in this node
		if (random_number == current->data.ordinance)
			print_challenge(current->data);
		current = current->next; //iteration
	}	
}
